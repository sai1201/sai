/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Jun 2015     Steven
 *
 */

//RAMSEY specific
var GLOBALS = {
    'dropShipExenseAccount': 202,
    'baseCostId': '1',
    'msrpId': '9',
    'mapId': '8',
    'baseImageFolderId': -4,
    'weight': {
        'lb': 1,
        'oz': 2
    },
    'RSR': {
        'folderId': 5048,
        'fileName': 'IM-QTY-CSV.csv',
        'locationId': 5,
        'accountId': 963,
        'productListing': 'http://www.rsrgroup.com/dealer/ftpdownloads/fulfillment-inv-new.txt',
        'imagePath': 'http://img.rsrgroup.com/pimages/',
        'rootInventoryItem': 810,
        'rootInventoryName': 'RSR',
        'vendorId': "234"
    }
};

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function rsrInventoryAdjustments(type) {

    var context = nlapiGetContext();
    context.setPercentComplete(0.00);

    //retrieve inventory file from file cabinet
    var results = getInventoryFiles(GLOBALS.RSR.folderId, GLOBALS.RSR.fileName);
    var contents = [];
    var adjustments = [];

    if (results) {
        //there should only be 1 file in the file cabinet, however in case we start running into NetSuite limitations and have to split up the file
        //we should support the possibility of multiple files
        results.forEach(function (e) {
            var id = e.getValue('internalid', 'file');
            var fileName = e.getValue('name', 'file');
            nlapiLogExecution('DEBUG', 'fileName', fileName);
            var file = nlapiLoadFile(id); //5 mb file size limitation
            var data = file.getValue();
            contents = contents.concat(CSVToArray(data, ','));
        });

        context.setPercentComplete(25);
        context.getPercentComplete();

        nlapiLogExecution('AUDIT', 'CSV Row Count', contents.length);

        if (contents.length > 0) {
            //get all the RSR inventory items and the QOH
            var items = getInventoryItemsWithExternalIdsByLocation(GLOBALS.RSR.locationId);

            if (Object.keys(items).length > 0) {
                nlapiLogExecution('AUDIT', 'NS Items witj RSR location length', Object.keys(items).length);

                //loop through the CSV data and determine if any inventory adjustments need to be made
                //for (var i = 0; i < contents.length; i++) {

                for (var i = 0; i < contents.length; i++) {
                    var externalid = contents[i][0];
                    var qty = parseFloat(contents[i][1] || 0);

                    if (externalid in items) {
                        if (qty != items[externalid].qoh) {
                            var adjQty = (items[externalid].qoh - qty) * -1;
                            if (adjQty != 0)
                                adjustments.push({ 'itemId': items[externalid].internalid, 'adjQty': adjQty });
                            //if want push the name also 'name': items[externalid].name,
                        }
                    }
                }
                context.setPercentComplete(50);
                context.getPercentComplete();
                nlapiLogExecution('AUDIT', 'Total Adjustments Found', Object.keys(adjustments).length);
                //nlapiLogExecution('DEBUG', 'adjustments', JSON.stringify(adjustments));

                /* there is a 5,000 sublist row limit,  we limit it to 2000 per sublist.
                   create 2000 records as line items in a single inventory adjustment record */

                var invchecklist = "";
                try {
                    var adjposition = 0;

                    for (var i = 0; i < Object.keys(adjustments).length / 2000; i++) {
                        //context.setPercentComplete((100 * i) / results.length);
                        //context.getPercentComplete();
                        // create new inventory record
                        var newAdjustment = nlapiCreateRecord('inventoryadjustment');
                        newAdjustment.setFieldValue('account', GLOBALS.RSR.accountId);
                        newAdjustment.setFieldValue('adjlocation', GLOBALS.RSR.locationId);

                        var subrecords = adjustments.slice(adjposition, adjposition + 2000);

                        // create new inventory adjustment line items and add them to record, MAX 2000 lines per record
                        for (j = 0; j < subrecords.length; j++) {
                            newAdjustment.selectNewLineItem('inventory');
                            newAdjustment.setCurrentLineItemValue('inventory', 'location', GLOBALS.RSR.locationId);
                            newAdjustment.setCurrentLineItemValue('inventory', 'item', subrecords[j].itemId);
                            newAdjustment.setCurrentLineItemValue('inventory', 'adjustqtyby', subrecords[j].adjQty);
                            newAdjustment.commitLineItem('inventory');
                        }
                        // handle the loop and skip variables
                        adjposition = adjposition + 2000;

                        // submit the record
                        var inventoryId = nlapiSubmitRecord(newAdjustment);

                        context.setPercentComplete(((100 * i) / (Object.keys(adjustments).length / 2000)) + 50);
                        context.getPercentComplete();

                        pauseScriptIfNecessary(context, 50);
                        invchecklist += 'Loop no : ' + i + ' -- new invAdj record Id : ' + inventoryId + '    **    ';
                    }
                    nlapiLogExecution('DEBUG', 'Complete details of process', JSON.stringify(invchecklist));
                    context.setPercentComplete(100);
                    context.getPercentComplete();
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'new inventoryId error : ', e.message);
                    nlapiSendEmail(118562, 'susant.sahu@srisoft9.com', 'Error Occured in rsrInventoryAdjustments', e.message);
                }
            }
        } else {
            nlapiLogExecution('ERROR', 'Empty RSR File', 'The RSR inventory file ' + GLOBALS.RSR.fileName + ' does not contain any data.');
            nlapiSendEmail(118562, 'susant.sahu@srisoft9.com', 'Error Occured in rsrInventoryAdjustments', 'The RSR inventory file ' + GLOBALS.RSR.fileName + ' does not contain any data.');
            throw nlapiCreateError('Empty RSR File', 'The RSR inventory file ' + GLOBALS.RSR.fileName + ' does not contain any data.');
        }

        context.setPercentComplete(100);
        context.getPercentComplete();
    } else {
        nlapiLogExecution('ERROR', 'RSR File Missing', 'The RSR inventory file ' + GLOBALS.RSR.fileName + ' cannot be found.');
        nlapiSendEmail(118562, 'susant.sahu@srisoft9.com', 'Error Occured in rsrInventoryAdjustments', 'The RSR inventory file ' + GLOBALS.RSR.fileName + ' cannot be found.');
        throw nlapiCreateError('RSR File Missing', 'The RSR inventory file ' + GLOBALS.RSR.fileName + ' cannot be found.');
    }
}

/**
  * Restlet to upload and initiate scheduled script to process file
  *
  * Data Format: {"type" : "RSR", "csvData" : ["ITEM 1, 10", "ITEM 2, 0"]}
  * Method: POST
  * Type: application/json
  */
function uploadInvAdjFileRestlet(datain) {
    nlapiLogExecution('DEBUG', 'In uploadInvAdjFileRestlet', nlapiGetContext().getName());

    //var json = JSON.parse(datain);
    var json = datain;
    var data = "";
    var ret = null;

    json.csvData.forEach(function (element, index) {
        data += element;

        //add a carriage return for all rows except for the last one
        if (index < json.csvData.length) {
            data += '\n';
        }
    });

    try {
        var name = "", folder = null;

        switch (json.type) {
            case 'RSR':
                name = GLOBALS.RSR.fileName;
                folder = GLOBALS.RSR.folderId;
                break;
            default:
                throw nlapiCreateError('Invalid File Type', 'The file type (' + json.type + ') is invalid.');
        }

        nlapiLogExecution('DEBUG', 'Creating File', '');
        var file = nlapiCreateFile(name, 'CSV', data);
        file.setFolder(folder);
        nlapiLogExecution('DEBUG', 'File Created', '');
        var id = nlapiSubmitFile(file);
        nlapiLogExecution('DEBUG', 'File Uploaded', id);

        ret = { "id": id, "name": name, "error": "" };

        nlapiLogExecution('DEBUG', 'Submitting Scheduled Script', '');
        var status = nlapiScheduleScript('customscript_rsr_inventory_adjustments', 'customdeploy_rsr_inventory_adjustments')
        nlapiLogExecution('DEBUG', 'uploadInvAdjFileRestlet complete', JSON.stringify(ret)
            + ' ------------------- status of ScheduleScript :  ' + status);

    } catch (e) {
        var error;

        if (e instanceof nlobjError) {
            nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
            error = e.getCode() + ': ' + e.getDetails();
        } else {
            nlapiLogExecution('ERROR', 'unexpected error', e.toString());
            error = e.toString();
        }

        ret = { "id": "", "name": name, "error": error };
    }

    //if ( status == 'QUEUED' )
    //    break;
    return JSON.stringify(ret);
}

function getRSRProductListing() {
    nlapiLogExecution('DEBUG', 'in getRSRProductListing', '');
    var context = nlapiGetContext();
    var updateSuccessCount = 0, newSuccessCount = 0, updateFailureCount = 0, newFailureCount = 0;
    var failedUpdateList = [], failedNewList = [];

    //get the product listing from RSR
    var response = nlapiRequestURL(GLOBALS.RSR.productListing, null, null, 'GET');

    nlapiLogExecution('DEBUG', 'RSR product file retrieved', '');

    if (response) {
        nlapiLogExecution('DEBUG', 'Parsing File', '');

        var start = new Date();
        //var lines = CSVToArray(response.getBody(), ';');
        var lines = Baby.parse(response.getBody(), {
            delimiter: ";",  // auto-detect
            newline: "",    // auto-detect
            header: false,
            dynamicTyping: false,
            preview: 0,
            encoding: "",
            worker: false,
            comments: false,
            step: undefined,
            complete: undefined,
            error: undefined,
            download: false,
            skipEmptyLines: true,
            chunk: undefined,
            fastMode: undefined,
            beforeFirstChunk: undefined,
        }).data;
        var end = new Date();

        var time = (end - start) / 1000;

        nlapiLogExecution('DEBUG', 'CSVToArray Time', time);
        nlapiLogExecution('DEBUG', 'RSR File Line Count', lines.length);

        //get all the RSR inventory items
        var items = getInventoryItemsByLocation(GLOBALS.RSR.locationId);

        for (var i = 0; i < lines.length; i++) {
            try {
                var rsrStockNo = lines[i][0];
                var upc = lines[i][1];
                var basePrice = lines[i][6];
                var msrp = lines[i][5];
                var map = lines[i][70];
                var fullManufacturerName = lines[i][10];
                var imageName = lines[i][14];
                var productDescription = lines[i][2];
                var inventoryQty = lines[i][8];
                var weight = lines[i][7];
                var item = null, imageId = null, parentItemId = null, parentName = "", parentDepartmentId = null, thumbName = "", manufacturerName = "", thumbnailId = null;
                var commit = false;

                if (imageName) {
                    thumbName = imageName.substr(0, imageName.lastIndexOf('.')) + '_thumb' + imageName.substr(imageName.lastIndexOf('.'));
                }

                //update the price on the current line if the price level of the current line is equal to price level we are looking for and the price is different from the currenly saved price
                var updatePrice = function (line, level, price) {
                    if (record.getLineItemValue('price', 'pricelevel', line) == level && price && parseFloat(record.getLineItemValue('price', 'price_1_', line)) != parseFloat(price)) {
                        nlapiLogExecution('AUDIT', record.getLineItemValue('price', 'pricelevelname', line), 'oldPrice: ' + record.getLineItemValue('price', 'price_1_', line) + ' | newPrice: ' + price);
                        record.setLineItemValue('price', 'price_1_', line, price);
                        commit = true;
                    }
                };

                //update the record field only if the value changed
                var updateField = function (fieldName, newValue, oldValue, overwrite) {
                    newValue = newValue || "";
                    oldValue = oldValue || "";
                    overwrite = overwrite || false;

                    nlapiLogExecution('DEBUG', fieldName, 'oldValue: ' + oldValue + ' | newValue: ' + newValue);

                    //fields should only be updated if the old value is null and the new value is populated, this will prevent overwritting any values that were manually set
                    //unless the overwrite parameter is true and then the value should be overwritten
                    if (((!overwrite && isNullOrEmpty(oldValue)) || overwrite) && newValue != oldValue) {
                        nlapiLogExecution('AUDIT', 'Updating ' + fieldName, 'newValue: ' + newValue + ' | oldValue: ' + oldValue);
                        record.setFieldValue(fieldName, newValue);
                        commit = true;
                    }
                }

                //try and locate product by external id
                if (rsrStockNo in items.externalIdMapping) {
                    item = items.item[items.externalIdMapping[rsrStockNo]];
                } else if (rsrStockNo in items.item) {
                    //try and locate product by item id
                    item = items.item[rsrStockNo];
                } else if (upc in items.upcMapping) {
                    //try and locate product by upc code
                    item = items.item[items.upcMapping[upc]];
                }

                if (!item) {
                    nlapiLogExecution('DEBUG', i, rsrStockNo);
                }

                //try and locate the parent item by search for an existing item id that is equal to the manufacturer of this item
                var results = findParentByManufacturerName(fullManufacturerName);

                if (results) {
                    parentItemId = results[0].getId();
                    parentName = results[0].getValue('itemid');
                    parentDepartmentId = results[0].getValue('department');
                } else {
                    parentItemId = GLOBALS.RSR.rootInventoryItem;
                    parentName = GLOBALS.RSR.rootInventoryName;
                    parentDepartmentId = nlapiLookupField('item', parentItemId, 'department');
                }

                if (fullManufacturerName) {
                    manufacturerName = fullManufacturerName;
                } else {
                    manufacturerName = parentName;
                }

                if (item) {
                    //Per David, the following fields are the only fields that should be updated on an item
                    //Unless explicitly configured, fields will not be overwritten if they already have a value in NS
                    nlapiLogExecution('DEBUG', 'Update Item', JSON.stringify(item));

                    var record = nlapiLoadRecord('inventoryitem', item.internalid);
                    updateField('externalid', rsrStockNo, record.getFieldValue('externalid'));
                    updateField('isonline', 'T', record.getFieldValue('isonline'), true);

                    //if the manfacturer field on the record is null, use the name of the parent if it is specified
                    if (!record.getFieldValue('manufacturer')) {
                        if (record.getFieldValue('parent')) {
                            manufacturerName = record.getFieldText('parent');
                        }
                    }

                    if (!isNullOrEmpty(weight) && weight != '0') {
                        //product weight should always overwrite with the value specified by RSR
                        updateField('weight', weight, record.getFieldValue('weight'), true);
                        updateField('weightunit', GLOBALS.weight.oz, record.getFieldValue('weightunit'), true);
                    } else {
                        //per David, if weight is 0 default to 1 lb so shipping can be calculated
                        updateField('weight', 1, record.getFieldValue('weight'), true);
                        updateField('weightunit', GLOBALS.weight.lb, record.getFieldValue('weightunit'), true);
                    }

                    //only update images if they have not been defined
                    if (isNullOrEmpty(record.getFieldValue('storedisplayimage'))) {
                        imageId = findOrUploadImage(imageName, manufacturerName, GLOBALS.RSR.imagePath);
                        updateField('storedisplayimage', imageId, record.getFieldValue('storedisplayimage'));
                    }

                    if (isNullOrEmpty(record.getFieldValue('storedisplaythumbnail'))) {
                        thumbnailId = findOrUploadImage(thumbName, manufacturerName, GLOBALS.RSR.imagePath);
                        updateField('storedisplaythumbnail', thumbnailId, record.getFieldValue('storedisplaythumbnail'));
                    }

                    for (var x = 1; x <= record.getLineItemCount('price') ; x++) {
                        updatePrice(x, GLOBALS.baseCostId, basePrice);
                        updatePrice(x, GLOBALS.msrpId, msrp);
                        updatePrice(x, GLOBALS.mapId, map);
                    }
                } else {
                    //create new item
                    commit = true;
                    nlapiLogExecution('DEBUG', 'Create Item', rsrStockNo);

                    var record = nlapiCreateRecord('inventoryitem');
                    record.setFieldValue('parent', parentItemId);
                    record.setFieldValue('externalid', rsrStockNo);
                    record.setFieldValue('itemid', rsrStockNo);
                    record.setFieldValue('upccode', upc);
                    record.setFieldValue('purchasedescription', productDescription);
                    record.setFieldValue('salesdescription', productDescription);
                    record.setFieldValue('storedescription', productDescription);
                    record.setFieldValue('storedetaileddescription', lines[i][13]);

                    if (fullManufacturerName) {
                        manufacturerName = fullManufacturerName;
                        record.setFieldValue('manufacturer', fullManufacturerName);
                    } else {
                        manufacturerName = parentName;
                        record.setFieldValue('manufacturer', parentName);
                    }

                    imageId = findOrUploadImage(imageName, manufacturerName, GLOBALS.RSR.imagePath);
                    thumbnailId = findOrUploadImage(thumbName, manufacturerName, GLOBALS.RSR.imagePath);

                    nlapiLogExecution('DEBUG', 'imageId', imageId);
                    nlapiLogExecution('DEBUG', 'thumbnailId', thumbnailId);

                    record.setFieldValue('mpn', lines[i][11]);
                    record.setFieldValue('cost', lines[i][6]);
                    record.setFieldValue('taxschedule', 1);

                    if (!isNullOrEmpty(weight) && weight != '0') {
                        record.setFieldValue('weight', weight);
                        record.setFieldValue('weightunit', GLOBALS.weight.oz);
                    } else {
                        //per David, if weight is 0 default to 1 lb so shipping can be calculated
                        record.setFieldValue('weight', 1);
                        record.setFieldValue('weightunit', GLOBALS.weight.lb);
                    }

                    record.setFieldValue('location', GLOBALS.RSR.locationId);
                    record.setFieldValue('preferredlocation', GLOBALS.RSR.locationId);
                    record.setFieldValue('storedisplayimage', imageId);
                    record.setFieldValue('storedisplaythumbnail', thumbnailId);

                    record.setFieldValue('dropshipexpenseaccount', GLOBALS.dropShipExenseAccount);
                    record.setFieldValue('department', parentDepartmentId);
                    record.setFieldValue('isonline', 'T');

                    //set the quantity onhand for the item in the RSR location
                    for (var x = 1; x <= record.getLineItemCount('locations') ; x++) {
                        if (parseInt(record.getLineItemValue('locations', 'location', x), 10) == GLOBALS.RSR.locationId) {
                            nlapiLogExecution('DEBUG', 'quantityavailable', inventoryQty);
                            record.setLineItemValue('locations', 'quantityavailable', x, inventoryQty);
                            break;
                        }
                    }

                    for (var x = 1; x <= record.getLineItemCount('price') ; x++) {
                        updatePrice(x, GLOBALS.baseCostId, basePrice);
                        updatePrice(x, GLOBALS.msrpId, msrp);
                        updatePrice(x, GLOBALS.mapId, map);
                    }

                    record.selectNewLineItem('itemvendor');
                    record.setCurrentLineItemValue('itemvendor', 'vendor', GLOBALS.RSR.vendorId);
                    record.setCurrentLineItemValue('itemvendor', 'preferredvendor', 'T');
                    record.commitLineItem('itemvendor');
                }

                if (commit) {
                    //commit the item
                    var id = nlapiSubmitRecord(record, true, true);
                    nlapiLogExecution('DEBUG', 'id', id);

                    if (item) {
                        updateSuccessCount++;
                    } else {
                        newSuccessCount++;
                    }
                }
            } catch (e) {
                if (item) {
                    updateFailureCount++;
                    failedUpdateList.push({
                        'itemName': rsrStockNo,
                        'id': item.internalid,
                        'message': e.getCode() + ': ' + e.getDetails()
                    });
                } else {
                    newFailureCount++;
                    failedNewList.push({
                        'itemName': rsrStockNo,
                        'message': e.getCode() + ': ' + e.getDetails()
                    });
                }
            }

            context.setPercentComplete((100 * i) / lines.length);
            context.getPercentComplete();
            pauseScriptIfNecessary(context, 200);
        } //end loop

        //generate email notification
        var subject = 'RSR Inventory Item Load Complete';
        var body = updateSuccessCount + ' items updated.<br />';
        body += newSuccessCount + ' items created.<br />';

        if (updateFailureCount > 0) {
            body += '<br />The following ' + updateFailureCount + ' items failed to be updated:';
            body += '<ul>';

            failedUpdateList.forEach(function (e) {
                var url = nlapiResolveURL('RECORD', 'inventoryitem', e.id);
                body += '<li><a href="' + url + '">' + e.itemName + '</a>:&nbsp;' + e.message + '</li>';
            });

            body += '</ul>';
        }

        if (newFailureCount > 0) {
            body += '<br />The following ' + newFailureCount + ' items failed to be created:';
            body += '<ul>';

            failedNewList.forEach(function (e) {
                body += '<li>' + e.itemName + ':&nbsp;' + e.message + '</li>';
            });

            body += '</ul>';
        }

        var end = new Date();
        var time = ((end - start) / 1000 / 60).toFixed(2);

        nlapiLogExecution('DEBUG', 'Execution Time', time);
        body += '<br /><br /><b>Execution Time: ' + time + ' minutes</b>';
        nlapiSendEmail(118562, 'ns.error@cmcgov.com', subject, body);
    }

}

function findParentByManufacturerName(name) {
    var columns = [];
    columns.push(new nlobjSearchColumn('itemid'));
    columns.push(new nlobjSearchColumn('department'));

    var filters = [];
    filters.push(new nlobjSearchFilter('itemid', null, 'is', name));
    filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

    return nlapiSearchRecord('item', null, filters, columns);
}

function findOrUploadImage(imageName, manufacturerName, imagePath) {
    var imageId = null, folderId = null;

    if (!imageName || !manufacturerName) {
        return null;
    }

    try {
        //searching for existing image by name
        var filters = [];
        filters.push(new nlobjSearchFilter('isavailable', null, 'is', 'T'));
        filters.push(new nlobjSearchFilter('name', null, 'is', imageName));

        var results = nlapiSearchRecord('file', null, filters, null);

        if (results && results.length == 1) {
            nlapiLogExecution('DEBUG', 'Image found', imageName);
            imageId = results[0].getId();
        } else if (results && results.length > 1) {
            nlapiLogExecution('ERROR', 'Multiple Images Found', 'More than 1 image found for ' + imageName);
        } else {
            //image not found, determine what folder the image should be uploaded to
            var filters = [];
            filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
            filters.push(new nlobjSearchFilter('name', null, 'is', manufacturerName));
            filters.push(new nlobjSearchFilter('parent', null, 'noneof', 5047));//ignore any folders under the Inventory Files folder

            var results = nlapiSearchRecord('folder', null, filters, null);

            if (results && results.length == 1) {
                nlapiLogExecution('DEBUG', 'Folder found', manufacturerName);
                folderId = results[0].getId();
            } else if (results && results.length > 1) {
                nlapiLogExecution('ERROR', 'Multiple Folders Found', 'More than 1 folder found for ' + manufacturerName);
            } else {
                //create the folder
                nlapiLogExecution('DEBUG', 'Creating folder', manufacturerName);
                var rec = nlapiCreateRecord('folder');
                rec.setFieldValue('parent', GLOBALS.baseImageFolderId);
                rec.setFieldValue('name', manufacturerName);
                folderId = nlapiSubmitRecord(rec, true, true);
                nlapiLogExecution('DEBUG', 'Folder Created', folderId);
            }

            //upload image to file cabinet
            nlapiLogExecution('DEBUG', 'Requesting Image URL', imagePath + imageName);
            var response = nlapiRequestURL(imagePath + imageName);
            nlapiLogExecution('DEBUG', 'Image Response', response.getCode());

            //if the image was found on the server, then add it to the file cabinet
            if (response.getCode() == "200") {
                var file = nlapiCreateFile(imageName, getFileTypeId(response.getHeader('Content-Type')), response.getBody());
                file.setFolder(folderId);
                file.setIsOnline(true);
                imageId = nlapiSubmitFile(file);
                nlapiLogExecution('DEBUG', 'Image Uploaded', imageId);
            }
        }
    } catch (e) {
        if (e instanceof nlobjError) {
            nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
        } else {
            nlapiLogExecution('ERROR', 'unexpected error', e.toString());
        }
    }

    return imageId;
}

function getInventoryItemsWithExternalIdsByLocation(locationId) {
    var columns = [];
    var quantityColumn = new nlobjSearchColumn('formulanumeric').setFormula('nvl({locationquantityonhand}, 0)');

    columns.push(new nlobjSearchColumn('internalid').setSort());
    columns.push(new nlobjSearchColumn('itemid'));
    columns.push(new nlobjSearchColumn('externalid'));
    columns.push(new nlobjSearchColumn('upccode'));
    columns.push(quantityColumn);

    var filters = [];
    filters.push(new nlobjSearchFilter('externalid', null, 'noneof', '@NONE@'));
    filters.push(new nlobjSearchFilter('inventorylocation', null, 'anyof', locationId));
    filters.push(new nlobjSearchFilter('location', null, 'anyof', locationId));
    /* Criteria for getting only Active Items */
    filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

    var results = executeSearch('item', filters, columns);
    var item = {};

    if (results) {
        results.forEach(function (e) {
            item[e.getValue('externalid')] = {
                'internalid': e.getId(),
                'name': e.getValue('itemid'),
                'qoh': parseFloat(e.getValue(quantityColumn))
            };
        });
    }

    return item;
}

function getFileTypeId(contentType) {
    switch (contentType.toLowerCase()) {
        case 'image/gif':
            return 'GIFIMAGE';
        case 'image/jpeg':
            return 'JPGIMAGE';
        case 'image/pjpeg':
            return 'PJPGIMAGE';
        case 'image/x-png':
            return 'PNGIMAGE';
        case 'image/tiff':
            return 'TIFFIMAGE';
        case 'image/x-xbitmap':
            return 'BMPIMAGE';
        default:
            return contentType;
    }
}

function getInventoryItemsByLocation(locationId) {
    var columns = [];
    columns.push(new nlobjSearchColumn('internalid').setSort());
    columns.push(new nlobjSearchColumn('itemid'));
    columns.push(new nlobjSearchColumn('externalid'));
    columns.push(new nlobjSearchColumn('upccode'));
    columns.push(new nlobjSearchColumn('storedisplayimage'));

    var filters = [];
    filters.push(new nlobjSearchFilter('location', null, 'anyof', locationId));

    var results = executeSearch('item', filters, columns);
    var ret = {
        'item': {},
        'externalIdMapping': {},
        'upcMapping': {}
    };

    if (results) {
        results.forEach(function (e) {
            var itemId = e.getValue('itemid');
            var extId = e.getValue('externalid');
            var upc = e.getValue('upccode');

            ret.item[itemId] = {
                'internalid': e.getId(),
                'externalId': extId,
                'upc': upc,
                'imageId': e.getValue('storedisplayimage')
            };

            if (extId) {
                if (!(extId in ret.externalIdMapping)) {
                    ret.externalIdMapping[extId] = itemId;
                }
            }

            if (upc) {
                if (!(upc in ret.upcMapping)) {
                    ret.upcMapping[upc] = itemId;
                } else {
                    nlapiLogExecution('ERROR', 'Duplicate UPC Found', 'Duplicate UPC (' + upc + ') found');
                }
            }
        });
    }

    return ret;
}

function getInventoryFiles(folderId, fileName) {
    var columns = [];
    columns[columns.length] = new nlobjSearchColumn('internalid', 'file');
    columns[columns.length] = new nlobjSearchColumn('name', 'file');

    var filters = [];
    filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'is', folderId);
    filters[filters.length] = new nlobjSearchFilter('name', 'file', 'startswith', fileName);

    return nlapiSearchRecord('folder', null, filters, columns);
}

function pauseScriptIfNecessary(context, units) {
    if (context.getRemainingUsage() <= units) {
        var state = nlapiYieldScript();

        if (state.status == 'FAILURE') {
            nlapiLogExecution('ERROR', 'Failed to yield script, exiting: Reason = ' + state.reason + ' / Size = ' + state.size);
            throw 'Failed to yield script';
        } else if (state.status == 'RESUME') {
            nlapiLogExecution('AUDIT', 'Resuming script because of ' + state.reason + '.  Size = ' + state.size);
        } else {
            nlapiLogExecution('DEBUG', 'Yield Status', state.status);
        }
    }
}

/**
 * This function will execute a search against the provided record using the provided
 * columns and filters and will continously re-execute until all records are retrieved
 * 
 * @param record
 * @param filters
 * @param columns
 * @param pauseScript
 * @returns
 */
function executeSearch(record, filters, columns, pauseScript) {
    if (!Array.isArray(filters)) {
        throw nlapiCreateError('Invalid Filter Format', 'The filter must be of type Array for executeSearch to work properly.');
    }

    var context = nlapiGetContext();
    pauseScript = pauseScript || false;

    var results = nlapiSearchRecord(record, null, filters, columns);
    var completeResultSet = results; //container of the complete result set
    var filterLength = filters.length;

    while (results && results.length == 1000) { //re-run the search if limit has been reached   
        var lastId = results[999].getValue('internalid'); //note the last record retrieved
        filters[filterLength] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastId); //create new filter to restrict the next search based on the last record returned
        results = nlapiSearchRecord(record, null, filters, columns);

        if (pauseScript) {
            pauseScriptIfNecessary(context, 50);
        }

        if (results) {
            completeResultSet = completeResultSet.concat(results); //add the result to the complete result set
        }
    }

    return completeResultSet;
}

function isNullOrEmpty(val) {
    return (val == null || val === '');
}

function deleteAdjustments() {
    var context = nlapiGetContext();
    context.setPercentComplete(0.00);

    var results = nlapiSearchRecord('inventoryadjustment', 'customsearch145', null, null);

    if (results) {
        nlapiLogExecution('DEBUG', 'Total Adjustments Found', results.length);

        for (var i = 0; i < results.length; i++) {
            nlapiDeleteRecord('inventoryadjustment', results[i].getId());

            context.setPercentComplete((100 * i) / results.length);
            context.getPercentComplete();
            pauseScriptIfNecessary(context, 50);
        }
    }
}

/**
 * Get externalID,InternalID from Items of Location RSRGroup.
 * This function will returns an array, get all internalIDs with key as ExternalIDs.
 *
 */
function getInventoryItemsWithExternalIdsByLocationID(locationId) {
    try {
        var columns = [];
        var quantityColumn = new nlobjSearchColumn('formulanumeric').setFormula('nvl({locationquantityonhand}, 0)');

        columns.push(new nlobjSearchColumn('internalid').setSort());
        columns.push(new nlobjSearchColumn('externalid'));
        columns.push(quantityColumn);

        var filters = [];
        filters.push(new nlobjSearchFilter('externalid', null, 'noneof', '@NONE@'));
        filters.push(new nlobjSearchFilter('inventorylocation', null, 'anyof', locationId));
        filters.push(new nlobjSearchFilter('location', null, 'anyof', locationId));
        /*Criteria for getting only Active Items */
        filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'))

        var results = executeSearch('item', filters, columns);
        /*Passing ExternalID as a key and gets Item InternalIDs*/
        var item = {};

        if (results) {
            results.forEach(function (e) {
                item[e.getValue('externalid')] = {
                    'internalid': e.getId()
                };
            });
        }
        return item;
    } catch (e) {
        nlapiLogExecution('DEBUG', ' getInventoryItemsWithExternalIdsByLocationID ', ' ' + e.message);
    }

}

/**
  * Inactivate all the RSR Inventory Items in the NS, by comparing RSRProdList and initiate scheduled script to process file
  *
  */
function InactivateRSRItemsInNetsuite() {
    try {
        nlapiLogExecution('AUDIT', 'InactivateRSRItemsInNetsuite:', 'Started');
        var context = nlapiGetContext();
        context.setPercentComplete(1.00);
        var externalidexist = '';
        var externaliddoesnotexist = '';

        //get all RSRProducts List
        var RSRItems = [];
        var response = nlapiRequestURL(GLOBALS.RSR.productListing, null, null, 'GET');
        nlapiLogExecution('DEBUG', 'RSR product file retrieved', 'Before Response');

        if (response) {
            nlapiLogExecution('DEBUG', 'Rseponse :', 'Started Response');
            nlapiLogExecution('DEBUG', 'Inside Response if condition, Parsing the File', '' + response);
            var start = new Date();
            ///reads the data from the ProductListing URL and populate all the data into Lines.
            var lines = Baby.parse(response.getBody(), {
                delimiter: ";",  // auto-detect
                newline: "",    // auto-detect
                header: false,
                dynamicTyping: false,
                preview: 0,
                encoding: "",
                worker: false,
                comments: false,
                step: undefined,
                complete: undefined,
                error: undefined,
                download: false,
                skipEmptyLines: true,
                chunk: undefined,
                fastMode: undefined,
                beforeFirstChunk: undefined,
            }).data;

            context.setPercentComplete(5.00);
            nlapiLogExecution('DEBUG', 'Lines Object' + ' ' + lines, '' + lines != null ? lines.length : lines);
            //Getting ExternalId[ItemName or ItemID] From the RSR Product List.TXT file 
            //Assigning to the RSRItems Array.
            for (var i = 0; lines != null && i < lines.length; i++) {
                try {
                    if (lines[i][0] == '' || null == lines[i][0]) {
                        nlapiLogExecution('DEBUG', 'Lines null or empty', ' ' + i);
                        continue;
                    }
                    RSRItems.push(lines[i][0]);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'RSR File Lines Count', e.message);
                }
            }
            nlapiLogExecution('DEBUG', 'Item Details ', 'RSRItems Length : ' + RSRItems.length);
            context.setPercentComplete(12.00);
            //get INTERNALID's from the RSR inventory items in NETSUITE by passing Key as EXTERNALID[refers to RSRListProucts ItemName/ExternalID]
            var NSItems = getInventoryItemsWithExternalIdsByLocationID(GLOBALS.RSR.locationId);
            nlapiLogExecution('DEBUG', 'Item Details ', 'NSItems Length : ' + Object.keys(NSItems).length);

            var ExistCount = 0;
            var NotExistCount = 0;
            //var externalidexist = '';
            var externalidnotexist = '';
            var NSItemsNotExistinRSR = [];

            for (NS in NSItems) {
                //nlapiLogExecution('DEBUG', 'item array object :' + NS, ' this rests: ' + NSItems[NS].internalid);
                if (RSRItems.indexOf(NS) >= 0) {
                    ExistCount++;
                    //externalidexist += NS + 'doesnot exist ';
                }
                else {
                    NotExistCount++;
                    externalidnotexist += 'NS: ' + NS + ' id: ' + NSItems[NS].internalid + ' ***************** ';
                    NSItemsNotExistinRSR.push(NSItems[NS].internalid);
                }

            }
            nlapiLogExecution('DEBUG', 'externalidexistCount in Netsuite Side:', ExistCount);
            nlapiLogExecution('DEBUG', 'externalidnotexistCount in Netsuite Side:', NotExistCount);
            //nlapiLogExecution('DEBUG', 'externalidnotexist records', externalidnotexist);

            nlapiLogExecution('DEBUG', 'Array Count NSItemsNotExistinRSR', NSItemsNotExistinRSR.length);
            var errorLog = '';
            var x = 13.00;
            for (var itemindex = 0; itemindex < NSItemsNotExistinRSR.length; itemindex++) {
                try {
                    //nlapiLogExecution('DEBUG', 'Items :' + NSItemsNotExistinRSR[itemindex]);
                    //Passing NS Inventory Item InternalID and load the data
                    var record = nlapiLoadRecord('inventoryitem', NSItemsNotExistinRSR[itemindex]);
                    //Setting the ISInactivate Field to True
                    record.setFieldValue('isinactive', 'T');
                    //commit/submitting the item record
                    var ItemId = nlapiSubmitRecord(record);
                    //nlapiLogExecution('DEBUG', 'ItemId :' + ItemId);
                    x += ((100 * itemindex) / NSItemsNotExistinRSR.length);
                    context.setPercentComplete(x);
                    context.getPercentComplete();
                    pauseScriptIfNecessary(context, 50);
                } catch (e) {
                    errorLog += 'ItemID :' + NSItems[NS].internalid + ' Exception :' + e + ' *** ';
                }
            }

            if (errorLog != '') {
                nlapiLogExecution('AUDIT', 'NSItemsNotExistinRSR Errors Inside For Loop:', errorLog);
                nlapiSendEmail(118562, 'susant.sahu@srisoft9.com,saikrishna.araju@srisoft9.com', 'Error Occured in InactivateRSRItemsInNetsuite', errorLog);
            }
            else {
                nlapiSendEmail(118562, 'susant.sahu@srisoft9.com,saikrishna.araju@srisoft9.com', 'Inactivate RSR Items In Netsuite process report', 'Inactivated below items in NS : ' + externalidnotexist);
            }
            nlapiLogExecution('DEBUG', 'Rseponse :', 'Ended Response');
        }
        nlapiLogExecution('AUDIT', 'InactivateRSRItemsInNetsuite:', 'Ended');
    } catch (e) {
        nlapiLogExecution('ERROR', 'InactivateRSRItemsInNetsuite:', e.message);
    }
}